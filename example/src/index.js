/**
 * A useless function to test plugin. It should be print hello world message: {{{{HELLO_WORLD}}}}
 * @param {string} argA - Argument A. In the param description: {{{{HELLO_WORLD}}}}
 * @param {Object} argB - Argument B
 * @returns {string} - In the returns description: {{{{HELLO_WORLD}}}}
 * @anUnknownTag {Typed} - In a unknown tag description : {{{{HELLO_WORLD}}}}
 * @anotherUnknownTag Without type : {{{{HELLO_WORLD}}}}
 */
function awesomeFunction (argA, argB) {
  return 'A random string...'
}

module.exports = awesomeFunction
