/* eslint-env node */
module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint'
  ],
  root: true,
  overrides: [
    {
      files: [
        './src/__tests__/**/*.{j,t}s?(x)'
      ],
      env: {
        jest: true
      },
      rules: {
        '@typescript-eslint/ban-ts-comment': 'off'
      }
    }
  ]
}
