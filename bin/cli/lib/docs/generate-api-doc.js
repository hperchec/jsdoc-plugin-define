const fs = require('fs-extra')
const path = require('path')
const juisy = require('@hperchec/juisy')

const {
  rootDir,
  $style,
  step,
  substep
} = juisy.utils

/**
 * Generate doc function
 * @param {Object} [options = {}] - The options object
 */
module.exports = async function generateAPIDoc (options = {}) {
  // Process options
  const distDir = options.distDir
  const outputDir = options.outputDir

  const outputFullPath = path.resolve(rootDir, distDir, outputDir)

  /**
   * Generate api documentation
   */
  step('Generating API documentation')

  // Here do logic to generate your documentation
  // ex: jsdoc
  fs.writeFileSync(path.resolve(outputFullPath, './api.md'), '# Awesome API !', { encoding: 'utf8' })

  substep($style.yellow('This command just creates an example file. Let\'s write your logic!'), { last: true })
}
