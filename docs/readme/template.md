# jsdoc-plugin-define

> *This file is generated with **@hperchec/readme-generator**. Don't edit it.*

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## 🚀 Get started

This JSDoc plugin is designed to dynamically inject global variables in doclets.

First, install package:

```sh
npm install -D @hperchec/jsdoc-plugin-define
```

In your jsdoc configuration file:

```js
module.exports = {
  plugins: [
    'node_modules/@hperchec/jsdoc-plugin-define'
  ],
  pluginOptions: {
    // Options for jsdoc-plugin-define
    define: {
      globals: {
        HELLO_WORLD: 'Hello world!'
      }
    }
  }
}
```

Let's use the `HELLO_WORLD` in your doc blocks:

```js
/**
 * A useless function. In the doclet description: {{{{HELLO_WORLD}}}}
 * @param {string} argA - Argument A. In the param description: {{{{HELLO_WORLD}}}}
 * @param {Object} argB - Argument B
 * @returns {string} - In the returns description: {{{{HELLO_WORLD}}}}
 * @anUnknownTag {Typed} - In a unknown tag description : {{{{HELLO_WORLD}}}}
 * @anotherUnknownTag Without type : {{{{HELLO_WORLD}}}}
 */
function awesomeFunction (argA, argB) {
  return 'A random string...'
}
```

JSDoc will replace the corresponding values in the output. Enjoy! 😊

### 🔧 Built-in `getOptions` util

This plugin embed a usefull `getOptions` method to extract plugin options from JSDoc configuration. So, in a JSDoc plugin context, you can use as follow:

```js
const getOptions = require('@hperchec/jsdoc-plugin-define/dist/utils/get-options')
// Example: get options for 'example' plugin:
getOptions('example') // => will read JSDoc configuration `pluginOptions.example` object
```

## 📄 License

<%= license %>

----

Made with ❤ by <%= author %>