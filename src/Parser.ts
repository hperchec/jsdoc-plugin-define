import escaperegexp from 'lodash.escaperegexp'

/**
 * Parser class.
 */
export class Parser {
  tokens = '{{{{path}}}}'
  tokensRegex = new RegExp(`{{{{(((?!{{{{).)+)}}}}`, 'g')
  globals: { 
    [key: string]: string
  } = {}

  constructor (options?) {
    if (options?.tokens) {
      this.setTokens(options.tokens)
    } else {
      this.setTokensRegex()
    }
    if (options?.globals) this.globals = options.globals
  }

  /**
   * Set tokens. Check if tokens is in valid format.
   * Example: `{{{{path}}}}`
   * @param {string} tokens - The tokens format using the "path" keyword
   * @returns {void}
   */
  setTokens (tokens: string) {
    const pathKeywordRegex = /^(.+)path(.+)$/
    if (tokens.match(pathKeywordRegex)) {
      this.tokens = tokens
      this.setTokensRegex()
    } else {
      throw new Error('JSDoc define plugin: "tokens" option is not valid')
    }
  }

  /**
   * Set token regexp
   * @returns {void}
   */
  setTokensRegex (): void {
    let [ tokenStart, tokenEnd ] = this.tokens.split('path')
    ;[ tokenStart, tokenEnd ] = [ escaperegexp(tokenStart), escaperegexp(tokenEnd) ] // escape regexp reserved characters
    this.tokensRegex = new RegExp(`${tokenStart}(((?!${tokenStart}).)+)${tokenEnd}`, 'g')
  }

  /**
   * Get matches in source.
   * @param {string} source - The source string to parse
   * @returns {RegExpMatchArray[]}
   */
  getMatches (source: string): RegExpMatchArray[] {
    return [...source.matchAll(this.tokensRegex)]
  }

  /**
   * Parse string and replace tokens by values defined in this.globals
   * @param {string} source - The string to parse
   * @returns {string}
   */
  parse (source: string): string {
    return source.replace(this.tokensRegex, (match, p1, offset, string) => { // eslint-disable-line @typescript-eslint/no-unused-vars
      return this.globals[p1] || match // if global is not defined, don't replacing
    })
  }
}

export default Parser
