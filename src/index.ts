/**
 * JSDoc "define" plugin
 * Similar to webpack define plugin.
 * Replace defined globals in description doclet.
 */
import { getOptions } from './utils/get-options'
import Parser from './Parser'

export interface PluginOptions {
  tokens?: string,
  globals?: { [key: string]: string }
}

// Get plugin options
const defineOptions: PluginOptions = getOptions('define')
const parser: Parser = new Parser(defineOptions)

/**
 * JSDoc plugin handlers
 */
export const handlers = {
  /**
   * Call plugin in "newDoclet" hook
   * @param e - The event. e.doclet will refer to the newly created doclet
   */
  newDoclet: function(e) {
    // Check in doclet description
    if (typeof e.doclet.description === 'string') {
      e.doclet.description = parser.parse(e.doclet.description)
    }
    // Check in doclet params
    if (e.doclet.params) {
      for (const index in e.doclet.params) {
        const param = e.doclet.params[index]
        if (typeof param.description === 'string') {
          param.description = parser.parse(param.description)
        }
      }
    }
    // Check in returns description
    if (e.doclet.returns) {
      for (const index in e.doclet.returns) {
        const _return = e.doclet.returns[index]
        if (typeof _return.description === 'string') {
          _return.description = parser.parse(_return.description)
        }
      }
    }
    // Check in unknown tags
    if (e.doclet.tags) {
      for (const index in e.doclet.tags) {
        const tag = e.doclet.tags[index]
        if (typeof tag.value === 'string') {
          tag.value = parser.parse(tag.value)
        }
      }
    }
  }
}
