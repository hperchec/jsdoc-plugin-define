import getOptions from '../../utils/get-options'

describe('Get JSDoc plugin options', () => {
  test('Options for "define" plugin to be defined', () => {
    const pluginOptions: any = getOptions('define')
    expect(pluginOptions.globals.HELLO_WORLD).toBe('Hello world!')
  })
  test('Call method without argument throws error', () => {
    expect(() => {
      // @ts-ignore
      getOptions()
    }).toThrow() // check if error message contains this string
  })
})
