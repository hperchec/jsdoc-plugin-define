import { Parser } from '../Parser'

const parserOptions = {
  tokens: '[[[[path]]]]',
  globals: {
    HELLO_WORLD: 'Hello world!'
  }
}
const source = `It should be print hello world message: [[[[HELLO_WORLD]]]]`

describe('Set tokens option', () => {
  const parser = new Parser(parserOptions)
  test('Tokens to be "[[[[path]]]]"', () => {
    expect(parser.tokens).toBe('[[[[path]]]]')
  })
})

describe('Set globals option', () => {
  const parser = new Parser(parserOptions)
  test('Global "HELLO_WORLD" to be "Hello world!"', () => {
    expect(parser.globals.HELLO_WORLD).toBe('Hello world!')
  })
})

describe('Get matches in source', () => {
  const parser = new Parser(parserOptions)
  test('Find 1 match', () => {
    const matchesInResult = parser.getMatches(source)
    expect(matchesInResult.length).toBe(1)
  })
})

describe('Parse', () => {
  const parser = new Parser(parserOptions)
  test('Replace "HELLO_WORLD" tokens by "Hello world!"', () => {
    const result = parser.parse(source)
    // Check in result if tokens have been replaced
    const matchesInResult = parser.getMatches(result)
    expect(matchesInResult.length).toBe(0)
  })
})
