module.exports = {
  conf: {
    plugins: [
      'node_modules/@hperchec/jsdoc-plugin-define'
    ],
    pluginOptions: {
      // Options for jsdoc-plugin-define
      define: {
        tokens: '{{{{path}}}}',
        globals: {
          HELLO_WORLD: 'Hello world!'
        }
      }
    }
  }
}