/* eslint-disable @typescript-eslint/no-var-requires */
const jsdocEnv = require('jsdoc/env')

/**
 * Get plugin options from jsdoc config:
 * Check if "pluginOptions" property is defined and get the corresponding value
 * @param {string} pluginKey - The plugin key
 * @returns {object}
 */
export function getOptions (pluginKey: string): object {
  if (!pluginKey) throw new Error(`JSDoc util "getOptions" expects string as first argument, got ${typeof pluginKey}`)
  let options = {}
  if (jsdocEnv.conf.pluginOptions) {
    options = jsdocEnv.conf.pluginOptions[pluginKey]
  }
  return options
}

export default getOptions
